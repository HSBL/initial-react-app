import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Post from "./components/Post";
import axios from "axios";

class App extends Component {
  state = {
    posts: [],
  };
  componentDidMount() {
    // PAKAI FETCH
    // fetch("https://jsonplaceholder.typicode.com/posts")
    //   .then((response) => response.json())
    //   .then((result) => this.setState({ posts: result }));

    // PAKAI AXIOS
    // axios
    //   .get("https://jsonplaceholder.typicode.com/todos")
    //   .then((response) => response.data)
    //   .then((result) => this.setState({ posts: result }));

    // AXIOS COPAS DARI POSTMAN
    var config = {
      method: "get",
      url: "http://localhost:4000/api/v1/players",
      headers: {},
    };

    axios(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        return response.data.data;
      })
      .then((result) => this.setState({ posts: result }))
      .catch(function (error) {
        console.log(error);
      });
  }
  render() {
    const { posts } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          {posts.map((postItem) => (
            <Post title={postItem.username} />
          ))}
        </header>
      </div>
    );
  }
}

export default App;
